<?php

use App\Downloads;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['authuser'])->group(function () {
    Route::get('/', function () {
        // Uses first & second Middleware
        return [
            'success' => true,
        ];
    });
    Route::post('/', function (Request $request) {
        // Uses first & second Middleware
        $urls = $request->input('urls');
        $folder_name = $request->input('folder_name');
        if (is_array($urls) or ($urls instanceof Traversable)) {
            foreach ($urls as $url_) {
                $url = new Downloads();
                $url->name = $folder_name;
                $url->url = $url_;
                $url->save();
            }
        }

        $urls = Downloads::get()->all();

        return [
            'success' => true,
            'data' => $urls,
        ];
    });

    Route::get('downloads', function () {
        $urls = Downloads::get()->all();
        foreach ($urls as $url_) {
            $url = $url_->url;
            $folder_name = $url_->name;
            $contents = file_get_contents($url);
            $name = substr($url, strrpos($url, '/') + 1);

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $data = curl_exec($curl);
            curl_close($curl);

            $success = false;
            if (!empty($data)) {
                $success = true;
                Storage::disk('public')->put($folder_name.'/'.$name, $data);
            }
            $url_->downloads_status = $success;
            $url_->save();
        }

        $urls = Downloads::get()->all();

        return [
            'success' => true,
            'data' => $urls,
        ];
    });

    Route::get('/d2', function () {
        $files = glob('/home/ubuntu/workspace/public/storage/2018-2/*');
        Zipper::make('public/tests.zip')->add($files)->close();
        Storage::deleteDirectory('2018-2');

        return 2;
    });
});
