<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ('QosmiO' !== $request->password) {
            return  response()->json([
    'success' => 'false',
]);
        }

        return $next($request);
    }
}
