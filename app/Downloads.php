<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Downloads.
 *
 * @property int                 $id
 * @property string              $name
 * @property string              $url
 * @property int                 $downloads_status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Downloads whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Downloads whereDownloadsStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Downloads whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Downloads whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Downloads whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Downloads whereUrl($value)
 * @mixin \Eloquent
 */
class Downloads extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'downloads_status',
    ];
}
